from quicktests import print_report, MiniTest, TestBase, get_report


class Test(TestBase):
    def test_one_equals_one(self):
        assert 1 == 1, "One does not equal one"

    def test_one_equals_one_str(self):
        assert "One" == "One", "\"One\" does not equal \"One\""

    def test_TestBase(self):
        class CustomTest(TestBase):
            def test_error(self):
                assert False

        t = CustomTest()
        assert "test_error" in get_report(t), "This tests detects errors"


if __name__ == '__main__':
    print_report(Test())

    def complex_test():
        return False

    print_report(
        MiniTest(
            test_true=[
                lambda: "test_custom" not in get_report(MiniTest(test_custom=[lambda: True,
                                                                              "This is True"])),
                "This test expects no error."
            ],
            test_false=[
                lambda: "test_custom" in get_report(MiniTest(test_custom=[lambda: False,
                                                                          "This is False"])),
                "This test expects an error."
            ]
        )
    )
